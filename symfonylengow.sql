-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 27 Juillet 2015 à 16:13
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `symfonylengow`
--
CREATE DATABASE IF NOT EXISTS `symfonylengow` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `symfonylengow`;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE IF NOT EXISTS `commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketplace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idFlux` int(11) NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_amount` double NOT NULL,
  `order_shipping` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Contenu de la table `commande`
--

INSERT INTO `commande` (`id`, `marketplace`, `idFlux`, `order_id`, `order_amount`, `order_shipping`) VALUES
(21, 'amazon', 88827, '111-2222222-3333333', 34.5, 5.5),
(22, 'amazon', 88827, '123-4567890-1112131', 115.5, 5.5),
(23, 'amazon', 88827, '222-1234567-1547225', 50.5, 5.5),
(26, 'cdiscount', 88829, '114KL55M', 29, 0),
(27, 'cdiscount', 88829, '11FGZ4SQ', 29, 5.5),
(28, 'test', 120, '20', 20, 20);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
