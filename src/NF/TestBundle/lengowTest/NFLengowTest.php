<?php

namespace NF\TestBundle\LengowTest;

class NFLengowTest
{
	private $url_orders;

	public function __construct($url_orders)
	{
    	$this->url_orders = $url_orders;
	}

  	/**
   	* Récupère un le flux xml
   	*
   	* @param object $container
   	* @return object
   	*/
	public function getFlux($container)
	{
      	// on convertit le fichier xml recupere en objet
      	$commandes = simplexml_load_file($this->url_orders);

      	// on retourne l objet recupere
      	return $commandes;
	}
}