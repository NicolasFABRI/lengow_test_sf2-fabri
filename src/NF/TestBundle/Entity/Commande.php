<?php

namespace NF\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * Commande
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="NF\TestBundle\Entity\CommandeRepository")
 * @GRID\Source(columns="id, marketplace, idFlux, orderId, orderAmount, orderShipping")
 */
class Commande
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=255)
     */
    private $marketplace;

    /**
     * @var integer
     *
     * @ORM\Column(name="idFlux", type="integer")
     */
    private $idFlux;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=255)
     */
    private $orderId;

    /**
     * @var float
     *
     * @ORM\Column(name="order_amount", type="float")
     */
    private $orderAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="order_shipping", type="float")
     */
    private $orderShipping;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return Commande
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set idFlux
     *
     * @param integer $idFlux
     * @return Commande
     */
    public function setIdFlux($idFlux)
    {
        $this->idFlux = $idFlux;

        return $this;
    }

    /**
     * Get idFlux
     *
     * @return integer 
     */
    public function getIdFlux()
    {
        return $this->idFlux;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return Commande
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set orderAmount
     *
     * @param float $orderAmount
     * @return Commande
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return float 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * Set orderShipping
     *
     * @param float $orderShipping
     * @return Commande
     */
    public function setOrderShipping($orderShipping)
    {
        $this->orderShipping = $orderShipping;

        return $this;
    }

    /**
     * Get orderShipping
     *
     * @return float 
     */
    public function getOrderShipping()
    {
        return $this->orderShipping;
    }
}
