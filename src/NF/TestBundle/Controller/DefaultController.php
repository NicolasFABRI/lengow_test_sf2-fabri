<?php

namespace NF\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('NFTestBundle:Default:index.html.twig', array('name' => $name));
    }
}
