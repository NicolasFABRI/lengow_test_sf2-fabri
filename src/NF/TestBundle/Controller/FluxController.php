<?php

namespace NF\TestBundle\Controller;

use NF\TestBundle\Entity\Commande;
use NF\TestBundle\Form\CommandeType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Yaml\Yaml;

class FluxController extends Controller
{
    public function indexAction(Request $request)
    {
    	// on appelle le service
    	$recupFlux = $this->container->get('nf_test.lengow_test');

    	// on definit le container
      	$container = $this->container;

      	// on appelle la fonction du service avec le conteneur en parametre
    	$commandes = $recupFlux->getFlux($container);

    	
    	// preparation de la requete recuperation de toutes les commandes
    	$repository = $this
    		->getDoctrine()
  			->getManager()
  			->getRepository('NFTestBundle:Commande')
		;

		// recuperation des commandes
		$listCommandes = $repository->findAll();

		// creation du tableau de stockage des ids de commandes deja existant dans la base de données
		$idExistants = array();

		// garnissage du tableau avec toutes les commandes deja en base
		foreach ($listCommandes as $commanderecups)
		{
  			$idCom = $commanderecups->getOrderId();
  			array_push($idExistants, $idCom);
		}

		// on parcoure toutes les commandes
    	foreach ($commandes->orders->order as $commande)
    	{
    		//Declaration et parametrage des variables necessaires pour le modele de la table Commande de la BDD
    		$marketplace = $commande->marketplace->__toString();
    		$idFlux = $commande->idFlux->__toString();
    		$order_id = $commande->order_id->__toString();
    		$order_amount = $commande->order_amount->__toString();
    		$order_shipping = $commande->order_shipping->__toString();

    		// recherche de la cle dans le tableau de cles existantes
    		$insertion = array_keys($idExistants, $order_id);

    		// si rien est retourne (pas de cles trouvee dans le tableau)
    		if(empty($insertion)){

    			// Création de l'entité
    			$commandeBD = new Commande();
    			$commandeBD->setMarketplace($marketplace);
    			$commandeBD->setIdFlux($idFlux);
    			$commandeBD->setOrderId($order_id);
    			$commandeBD->setOrderAmount($order_amount);
    			$commandeBD->setOrderShipping($order_shipping);

    			// On récupère l'EntityManager
    			$em = $this->getDoctrine()->getManager();

    			// Étape 1 : On « persiste » l'entité
    			$em->persist($commandeBD);

    			// Étape 2 : On « flush » tout ce qui a été persisté avant
    			$em->flush();
    		}
    	}

        // Utilisation du bundle de grid avec comme source l'entite Commande
        $source = new Entity('NFTestBundle:Commande');

        // Get a Grid instance
        $grid = $this->get('grid');

        // Attach the source to the grid
        $grid->setSource($source);

        // Return the response of the grid to the template
        return $grid->getGridResponse('NFTestBundle:Flux:index.html.twig');
    }

    public function addAction(Request $request)
    {
        // Creation du formulaire d ajout d une commande
        $newCommande = new Commande;
        $form = $this->get('form.factory')->create(new CommandeType, $newCommande);

        // si le formulaire est valide
        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($newCommande);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Commande bien enregistrée.');

            return $this->redirect($this->generateUrl('nf_test_flux'));
        }

        return $this->render('NFTestBundle:Flux:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function apiAction()
    {
        // preparation de la requete recuperation de toutes les commandes
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('NFTestBundle:Commande')
        ;

        // recuperation des commandes
        $listCommandes = $repository->findAll();

        //creation du tableau
        $commandesAPI = array();

        // creation de la reponse json
        $response = new JsonResponse();

        // garnissage du tableau a envoyer
        foreach ($listCommandes as $commande)
        {
            $commandeAPI = array(
                'id' => $commande->getId(),
                'marketplace' => $commande->getMarketPlace(),
                'idFlux' => $commande->getIdFlux(),
                'orderId' => $commande->getOrderId(),
                'orderAmount' => $commande->getOrderAmount(),
                'orderShipping' => $commande->getOrderShipping(),
            );

            array_push($commandesAPI, $commandeAPI);
        }

        $response->setData($commandesAPI);

        return $response;
    }

    public function apiDetailsAction($id_order)
    {
        // preparation de la requete recuperation de toutes les commandes
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('NFTestBundle:Commande')
        ;

        // recuperation de la commande avec le order id en parametre
        $commandeSpec = $repository->findOneBy(array('orderId' => $id_order));

        // creation de la reponse JSON
        $response = new JsonResponse();

        // ajout des donnees
        $response->setData(array(
            'id' => $commandeSpec->getId(),
            'marketplace' => $commandeSpec->getMarketPlace(),
            'idFlux' => $commandeSpec->getIdFlux(),
            'orderId' => $commandeSpec->getOrderId(),
            'orderAmount' => $commandeSpec->getOrderAmount(),
            'orderShipping' => $commandeSpec->getOrderShipping(),
        ));

        return $response;
    }

    public function formatYamlAction()
    {
        // preparation de la requete recuperation de toutes les commandes
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('NFTestBundle:Commande')
        ;

        // recuperation des commandes
        $listCommandes = $repository->findAll();

        //creation du tableau
        $commandesYAML = array();

        // creation de la reponse json
        $response = new JsonResponse();

        // garnissage du tableau a envoyer
        foreach ($listCommandes as $commande)
        {
            $commandeAPI = array(
                'id' => $commande->getId(),
                'marketplace' => $commande->getMarketPlace(),
                'idFlux' => $commande->getIdFlux(),
                'orderId' => $commande->getOrderId(),
                'orderAmount' => $commande->getOrderAmount(),
                'orderShipping' => $commande->getOrderShipping(),
            );

            array_push($commandesYAML, $commandeAPI);
        }

        $yaml = new Yaml();

        $formatYamlCommandes = $yaml->dump($commandesYAML);

        return $this->render('NFTestBundle:Flux:formatYAML.html.twig', array(
            'formatYamlCommandes' => $formatYamlCommandes,
        ));
    }
}
